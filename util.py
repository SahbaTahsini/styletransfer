from __future__ import division
import torch
from torch.utils.serialization import load_lua
import torchvision.transforms as transforms
import numpy as np
import argparse
import time
import os
from PIL import Image
from model import decoder1,decoder2,decoder3,decoder4,decoder5
from model import encoder1,encoder2,encoder3,encoder4,encoder5
import torch.nn as nn


class WCT(nn.Module):
    def __init__(self,args):
        super(WCT,self).__init__()

    #load pre-trained network
        vgg1=load_lua(args.vgg1)
        decoder1_torch=load_lua(args.decoder1)
        vgg2=load_lua(args.vgg2)
        decoder2_torch=load_lua(args.decoder2)

        vgg3=load_lua(args.vgg3)
        decoder3_torch=load_lua(args.decoder3)
        
        vgg4=load_lua(args.vgg4)
        decoder4_torch=load_lua(args.decoder4)

        vgg5=load_lua(args.vgg5)
        decoder5_torch=load_lua(args.decoder5)
    
        self.e1 = encoder1(vgg1)
        self.d1 = decoder1(decoder1_torch)
        self.e2 = encoder2(vgg2)
        self.d2 = decoder2(decoder2_torch)
        self.e3 = encoder3(vgg3)
        self.d3 = decoder3(decoder3_torch)
        self.e4 = encoder4(vgg4)
        self.d4 = decoder4(decoder4_torch)
        self.e5 = encoder5(vgg5)
        self.d5 = decoder5(decoder5_torch)
    
    
    def Whiten_and_color(self,cf,sf):
        cfSize=cf.size()
        c_mean=torch.mean(cf,1)
        c_mean=c_mean.unsqueeze(1).expand_as(cf)
        cf=cf-c_mean
        
        #fcfc
        contentConv=torch.mm(cf,cf.t()).div(cfSize[1]-1) +torch.eye(cfSize[0]).double() #*eps
        
        sfSize=sf.size()
        s_mean=torch.mean(sf,1)
        sf=sf-s_mean.unsqueeze(1).expand_as(sf)
        
        #sfsf
        styleConv=torch.mm(sf,sf.t()).div(sfSize[1]-1)+torch.eye(cfSize[0]).double()#*eps 
                                    
        c_u,c_e,c_v= torch.svd(contentConv,some=False)
        s_u,s_e,s_v=torch.svd(styleConv,some=False)
        
        k_c=cfSize[0]
        for i in range(cfSize[0]):
            if c_e[i]<0.00001:
                k_c=i
                break
                
        k_s=sfSize[0]
        for i in range(sfSize[0]):
            if s_e[i]<0.00001:
                k_s=i
                break
               
        c_d=(c_e[0:k_c]).pow(-0.125)
        c1=torch.mm(c_v[:,0:k_c],torch.diag(c_d)) #c_d * e_c
        c2=torch.mm(c1,(c_v[:,0:k_c].t()))  #* e_c.t()
        cf_hat=torch.mm(c2,cf)
        
        s_d=(c_e[0:k_s]).pow(0.125)
        s1=torch.mm(s_v[:,0:k_s],torch.diag(s_d)) #c_e*c_d
        s2=torch.mm(s1,(s_v[:,0:k_s].t())) #*c_e.t()
        fs_hat=torch.mm(s2,cf_hat)
        fs_hat=fs_hat +s_mean.unsqueeze(1).expand_as(fs_hat)
        return fs_hat
    
    def Transform(self,cf,sf,csF,alpha):
        cf=cf.double()
        sf=sf.double()
        C,W,H=cf.size(0),cf.size(1),cf.size(2)
        _,W1,H1=sf.size(0),sf.size(1),sf.size(2)
        cfView=cf.view(C,-1)
        sfView=sf.view(C,-1)
        
        fs_hat=self.Whiten_and_color(cfView,sfView)
        fs_hat=fs_hat.view_as(cf)
        fcs_hat=alpha*fs_hat + (1- alpha)*cf
        
        fcs_hat=fcs_hat.float().unsqueeze(0)
        csF.data.resize_(fcs_hat.size()).copy_(fcs_hat)
        return csF
        